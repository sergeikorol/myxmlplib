﻿/************************************************************************
  Sergei Korol 20014 All Rights Reserved

  Example of Classes for  integration with a processor, which uses XML formatted web API
  
  Created models for a request and response, serializable to XML
  Created helper class with 
   - method to process xml formatted request/response, 
     using settings stored in web.config 
   - method to integrate with current system, using MyTestTransactionData class
 
  This code is for demonstration purposes only,
  as an example of practical use of XML serialization/deserializion for a model class

*************************************************************************/
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Configuration;

namespace MyXmlPLib.Models
{

    //-------------------------------------------------------------
    // this class represents the XML format of a request to the Processor
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("TRANSACTION")]
    public class XMLPRequest
    {
        public XMLPRequest()
        {
            ClientID = string.Empty;
        }

        [System.Xml.Serialization.XmlElement("CLIENT_ID")]
        public string ClientID { get; set; }

        [System.Xml.Serialization.XmlElement("MERCHANTKEY")]
        public string MerchantKey { get; set; }

        [System.Xml.Serialization.XmlElement("USER_ID")]
        public string UserID { get; set; }

        [System.Xml.Serialization.XmlElement("USER_PW")]
        public string UserPW { get; set; }

        [System.Xml.Serialization.XmlElement("FUNCTION_TYPE")]
        public string FunctionType { get; set; }

        [System.Xml.Serialization.XmlElement("COMMAND")]
        public string Command { get; set; }

        [System.Xml.Serialization.XmlElement("PAYMENT_TYPE")]
        public string PaymentType { get; set; }

        [System.Xml.Serialization.XmlElement("CUSTOMER_CODE")]
        public string CustomerID { get; set; }

        [System.Xml.Serialization.XmlElement("INVOICE")]
        public string InvoiceID { get; set; }

        [System.Xml.Serialization.XmlElement("PURCHASE_ID")]
        public string PurchaseID { get; set; }

        [System.Xml.Serialization.XmlElement("ACCT_NUM")]
        public string AccountNum { get; set; }

        [System.Xml.Serialization.XmlElement("TRACK_DATA")]
        public string TrackData { get; set; }

        [System.Xml.Serialization.XmlElement("EXP_MONTH")]
        public string ExpMonth { get; set; }

        [System.Xml.Serialization.XmlElement("EXP_YEAR")]
        public string ExpYear { get; set; }

        [System.Xml.Serialization.XmlElement("TRANS_AMOUNT")]
        public string TransAmount { get; set; }

        [System.Xml.Serialization.XmlElement("PRESENT_FLAG")]
        public string PresentFlag { get; set; }

        [System.Xml.Serialization.XmlElement("CARDHOLDER")]
        public string Cardholder { get; set; }

        [System.Xml.Serialization.XmlElement("CVV2_CODE")]
        public string CVV2Code { get; set; }

        [System.Xml.Serialization.XmlElement("PIN_BLOCK")]
        public string PinBlock { get; set; }

        [System.Xml.Serialization.XmlElement("KEY_SERIAL_NUMBER")]
        public string KeySerialNumber { get; set; }

        [System.Xml.Serialization.XmlElement("TROUTD")]
        public string TRoutD { get; set; }

        [System.Xml.Serialization.XmlElement("CUSTOMER_STREET")]
        public string CustomerStreet { get; set; }

        [System.Xml.Serialization.XmlElement("CUSTOMER_ZIP")]
        public string CustomerZIP { get; set; }

        public string SerializeRequest()
        {
            System.Xml.Serialization.XmlSerializer xr = new System.Xml.Serialization.XmlSerializer(this.GetType());
            StringWriter stw = new StringWriter();
            XmlWriter xmw = XmlWriter.Create(stw);
            xr.Serialize(xmw, this);
            return stw.ToString();
        }

        public XMLPResponse ParseResponseXml(string xmlstr)
        {
            XMLPResponse response = new XMLPResponse();
            // fill up with error values by default
            response.TerminationStatus = "ERROR";
            response.Result = "ERROR";
            response.ResultCode = "98";
            response.InvoiceID = InvoiceID;
            response.TransAmount = TransAmount;
            response.TransDate = DateTime.UtcNow.ToString("mm/dd/yyyy HH:MM:ss");
            response.ResponseText = "INVALID RESPONSE FORMAT : 98 ";

            if (!String.IsNullOrWhiteSpace(xmlstr))
            {
                System.Xml.Serialization.XmlSerializer xr = new System.Xml.Serialization.XmlSerializer(response.GetType());
                StringReader sr = new StringReader(xmlstr.Replace("\n\r", ""));
                response = (XMLPResponse)xr.Deserialize(sr);
                sr.Close();
            }
            return response;
        }

        public XMLPRequest(string client_id, string merchant_key, string user_id, string user_pw)
        {
            if (!String.IsNullOrWhiteSpace(client_id))
            {
                ClientID = client_id;
            }
            if (!String.IsNullOrWhiteSpace(merchant_key))
            {
                MerchantKey = merchant_key;
            }
            if (!String.IsNullOrWhiteSpace(user_id))
            {
                UserID = user_id;
            }
            if (!String.IsNullOrWhiteSpace(user_pw))
            {
                UserPW = user_pw;
            }
        }
    }

    //-------------------------------------------------------------
    // this class represents the XML format a response from the Processor
    [Serializable()]
    [System.Xml.Serialization.XmlRoot("RESPONSE")]
    public class XMLPResponse
    {
        [System.Xml.Serialization.XmlElement("CLIENT_ID")]
        public string ClientID { get; set; }
        [System.Xml.Serialization.XmlElement("COMMAND")]
        public string Command { get; set; }
        [System.Xml.Serialization.XmlElement("PAYMENT_MEDIA")]
        public string PaymentTMedia { get; set; }
        [System.Xml.Serialization.XmlElement("PAYMENT_TYPE")]
        public string PaymentType { get; set; }
        [System.Xml.Serialization.XmlElement("INVOICE")]
        public string InvoiceID { get; set; }

        [System.Xml.Serialization.XmlElement("AUTH_AMOUNT")]
        public string AuthAmount { get; set; }
        [System.Xml.Serialization.XmlElement("AUTH_CODE")]
        public string AuthCode { get; set; }
        [System.Xml.Serialization.XmlElement("CTROUTD")]
        public string CtrOutD { get; set; }
        [System.Xml.Serialization.XmlElement("INTRN_SEQ_NUM")]
        public string InternalSeqNum { get; set; }
        [System.Xml.Serialization.XmlElement("REFERENCE")]
        public string Reference { get; set; }
        [System.Xml.Serialization.XmlElement("RESPONSE_TEXT")]
        public string ResponseText { get; set; }
        [System.Xml.Serialization.XmlElement("RESULT")]
        public string Result { get; set; }
        [System.Xml.Serialization.XmlElement("RESULT_CODE")]
        public string ResultCode { get; set; }
        [System.Xml.Serialization.XmlElement("TERMINATION_STATUS")]
        public string TerminationStatus { get; set; }
        [System.Xml.Serialization.XmlElement("TRACE_CODE")]
        public string TraceCode { get; set; }
        [System.Xml.Serialization.XmlElement("TRANS_AMOUNT")]
        public string TransAmount { get; set; }
        [System.Xml.Serialization.XmlElement("TRANS_DATE")]
        public string TransDate { get; set; }

        [System.Xml.Serialization.XmlElement("TRANS_SEQ_NUM")]
        public string TransSeqNum { get; set; }

        [System.Xml.Serialization.XmlElement("TRANS_TIME")]
        public string TransTime { get; set; }

        [System.Xml.Serialization.XmlElement("TROUTD")]
        public string TrOUTD { get; set; }

        [System.Xml.Serialization.XmlElement("LPTOKEN")]
        public string LpToken { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_ACCT_NUM")]
        public string DupAccountNum { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_AUTH_CODE")]
        public string DupAuthCode { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_CTROUTD")]
        public string DupCtrOUTD { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_INVOICE")]
        public string DupInvoice { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_PAYMENT_MEDIA")]
        public string DupPaymentMedia { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_TRANS_AMOUNT")]
        public string DupTransAmount { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_TRANS_DATE")]
        public string DupTransDate { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_TRANS_TIME")]
        public string DupTransTime { get; set; }

        [System.Xml.Serialization.XmlElement("DUP_TROUTD")]
        public string DupTrOUTD { get; set; }

        [System.Xml.Serialization.XmlElement("ORIG_TRANS_AMOUNT")]
        public string OrigTransAmount { get; set; }

        [System.Xml.Serialization.XmlElement("DIFF_AMOUNT_DUE")]
        public string DiffAmountDue { get; set; }

        [System.Xml.Serialization.XmlElement("DIFF_AUTH_AMOUNT")]
        public string DiffAuthAmount { get; set; }

        [System.Xml.Serialization.XmlElement("AVAIL_BALANCE")]
        public string AvailBalance { get; set; }

        [System.Xml.Serialization.XmlElement("ORIG_REQUEST_AMOUNT")]
        public string OrigRequestAmount { get; set; }

        [System.Xml.Serialization.XmlElement("APPROVED_AMOUNT")]
        public string ApprovedAmount { get; set; }

    }

    //-------------------------------------------------------------
    // an example of an internal transaction
    // the number of fields addresses the compatibility with legacy applications
    // and may be not required
    public class MyTestTransactionData  
    {
        public string application_id { get; set; }    //
        public string application_pin { get; set; }   //
        // references from an application
        public string transaction_id { get; set; }
        public string transaction_reference { get; set; }
        public string transaction_approval_code { get; set; }
        public string request_id { get; set; }
        public string customer_id { get; set; }
        public string client_id { get; set; }               
        public string merchant_profile { get; set; }        
        public string terminal_id { get; set; }             
        public string employee_id { get; set; }
        // card related data
        public string entry_mode { get; set; }
        public string card_number { get; set; }
        public string card_holder { get; set; }
        public string card_month { get; set; }
        public string card_year { get; set; }
        public string card_avs_data { get; set; }       // an AVS data if present, xml format - <PHONE>..10 digits billing phone..</PHONE>
        public string card_cvv_data { get; set; }
        public string card_issuer { get; set; }
        public string card_track1 { get; set; }
        public string card_track2 { get; set; }
        public string card_pin_info { get; set; }
        // transaction related data
        public string transaction_type { get; set; }    // credit/debit
        public int action_id { get; set; }           // auth, capture, auth&capture, void/refund
        public string amount_total { get; set; }
        public string local_datestamp { get; set; }
        public string card_pin_block { get; set; }
        public string status { get; set; }
        public string reference_no { get; set; }
        public string authorization_no { get; set; }
        public string response_status { get; set; }
        public string msg { get; set; }
        public string pwc_approval_code { get; set; }
        public string pwc_transaction_id { get; set; }
        public string pwc_transaction_timestamp { get; set; }
        public string pwc_service_transaction_id { get; set; }
        public string pwc_status_code { get; set; }
        public string pwc_status_message { get; set; }
    }

    //-------------------------------------------------------------
    // Helper class to process requests and responses
    //-------------------------------------------------------------
    public static class XmlProcessorHelper
    {
        //-------------------------------------------------------------
        // properties for XML Processor are stored in web.config
        public static string GetPWCPropertySettings(string property_name)
        {
            NameValueCollection section =
                (NameValueCollection)ConfigurationManager.GetSection("processor_clients_configurations");
            return section[property_name];
        }

        public static string PWCServerHost = ConfigurationManager.AppSettings["ProcessorServerHost"];
        public static string PWCServerHostBackup = ConfigurationManager.AppSettings["ProcessorServerHostBackup"];
        public static string PWCServerHostDemo = ConfigurationManager.AppSettings["ProcessorServerHostDemo"];

        //-------------------------------------------------------------
        // Method to process a request to XMLProcessor 
        public static XMLPResponse ProcessRequest(ref XMLPRequest processor_request, ref string response_str)
        {
            string request_str = processor_request.SerializeRequest();
            response_str = string.Empty;
            if (!String.IsNullOrWhiteSpace(request_str))
            {
                try
                {
                    WebClient wc = new WebClient();
                    wc.Headers.Add("Content-Type", "text/xml");
                    response_str = wc.UploadString(PWCServerHost, request_str);
                }
                catch
                {  // if network exception - try backup url
                    try
                    {
                        WebClient wc = new WebClient();
                        wc.Headers.Add("Content-Type", "text/xml");
                        response_str = wc.UploadString(PWCServerHostBackup, request_str);
                    }
                    catch
                    {
                        // error 98 will be returned by default. No details needed on this, as the error is not recoverable
                    }
                }
            }
            return processor_request.ParseResponseXml(response_str);
        }

        //-------------------------------------------------------------
        // example of routine to process
        public static void ProcessXMLPTransaction(ref MyTestTransactionData internal_card_data)
        {
            // build new XMLPRequest, based on information from CCCATransactionData
            string pwc_settings_rec = GetPWCPropertySettings(internal_card_data.merchant_profile);
            string[] pwc_settings = pwc_settings_rec.Split('|');
            XMLPRequest processor_request = new XMLPRequest(pwc_settings[0], pwc_settings[1], pwc_settings[2], pwc_settings[3]);
            //----------------------------------------------------------------------------
            processor_request.FunctionType = "PAYMENT";
            processor_request.TransAmount = internal_card_data.amount_total.ToString();
            processor_request.InvoiceID = internal_card_data.request_id; // 
            //----------------------------------------------------------------------------
            processor_request.PresentFlag = "2";  // Card present, keyed
            if ((internal_card_data.entry_mode == "SWIPED") || (!String.IsNullOrWhiteSpace(internal_card_data.card_track2)))
            {
                processor_request.PresentFlag = "3";  // Card present, swiped
                processor_request.TrackData = internal_card_data.card_track2;
            }
            else
            {
                processor_request.AccountNum = internal_card_data.card_number;
                processor_request.ExpMonth = internal_card_data.card_month;
                processor_request.ExpYear = internal_card_data.card_year;
                if (internal_card_data.application_id == "0")
                {   // Legacy System provides CVV code for keyed transactions, take it
                    processor_request.CVV2Code = internal_card_data.card_cvv_data;
                }
            }
            //----------------------------------------------------------------------------
            switch (internal_card_data.action_id)
            {
                case 1:
                    processor_request.Command = "PRE_AUTH";
                    processor_request.PaymentType = "CREDIT";
                    break;
                case 2:// post-auth capture, void or refund - need to provide the original reference
                    processor_request.Command = "COMPLETION";
                    processor_request.PaymentType = "CREDIT";
                    processor_request.TRoutD = internal_card_data.transaction_approval_code;
                    break;
                case 3:
                    processor_request.Command = "SALE";
                    processor_request.PaymentType = "CREDIT";
                    break;
                case 4:// debit transaction - pin block has to be supplied
                    processor_request.Command = "SALE";
                    processor_request.PaymentType = "DEBIT";
                    processor_request.PinBlock = internal_card_data.card_pin_block.Substring(0, 16);
                    processor_request.KeySerialNumber = internal_card_data.card_pin_block.Substring(16, 16);
                    break;
                case 5:
                    processor_request.Command = "VOID";
                    processor_request.PaymentType = "CREDIT";
                    processor_request.TRoutD = internal_card_data.transaction_approval_code;
                    processor_request.TrackData = string.Empty;
                    break;
                case 6:
                    processor_request.Command = "REFUND";
                    processor_request.PaymentType = "DEBIT";
                    processor_request.TRoutD = internal_card_data.transaction_approval_code;
                    processor_request.PinBlock = internal_card_data.card_pin_block.Substring(0, 16);
                    processor_request.KeySerialNumber = internal_card_data.card_pin_block.Substring(16, 16);
                    processor_request.TrackData = string.Empty;
                    break;
                default:
                    break;
            }

            //----------------------------------------------------------------------------
            string processor_response_str = string.Empty;
            XMLPResponse pwc_response = ProcessRequest(ref processor_request, ref processor_response_str);
            //----------------------------------------------------------------------------

            if (pwc_response.TerminationStatus == "SUCCESS")
            {
                internal_card_data.status = "1";
            }
            else
            {
                internal_card_data.status = "-1";
            }

            internal_card_data.reference_no = pwc_response.TransSeqNum;
            internal_card_data.authorization_no = pwc_response.AuthCode;
            string status_code = "96";
            try
            {
                status_code = pwc_response.ResponseText.Split(new Char[] { ':' })[1].Trim();
                internal_card_data.response_status = status_code;
                if (status_code.Length == 3) { internal_card_data.response_status = status_code.Substring(1, 2); }
            }
            catch { }

            internal_card_data.msg = processor_response_str;
            internal_card_data.pwc_approval_code = pwc_response.TrOUTD;
            internal_card_data.pwc_transaction_id = pwc_response.TransSeqNum;
            internal_card_data.pwc_transaction_timestamp = pwc_response.TransDate + " " + pwc_response.TransTime;
            internal_card_data.pwc_service_transaction_id = pwc_response.InternalSeqNum;
            if (pwc_response.ResultCode == "")
            {
                internal_card_data.pwc_status_code = pwc_response.ResultCode;
            }
            else
            {
                internal_card_data.pwc_status_code = "";
            }
            internal_card_data.pwc_status_message = pwc_response.ResponseText;
        }
    }
}